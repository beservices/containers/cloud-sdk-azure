FROM debian:latest

LABEL description="Image with Azure SDK plus terraform installation"

# Surpress Upstart errors/warning
RUN dpkg-divert --local --rename --add /sbin/initctl
RUN ln -sf /bin/true /sbin/initctl

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get install -y --no-install-recommends bash apt-transport-https ca-certificates \
    vim at curl gnupg net-tools git ssh-client tree groff jq lsb-release unzip && \
    rm -rf /var/lib/apt/list/*

RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -  && \
    echo "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee -a /etc/apt/sources.list.d/hashicorp.list && \
    apt-get update &&  apt-get install -y terraform && \
    curl -sL https://aka.ms/InstallAzureCLIDeb | bash && \
    rm -rf /var/lib/apt/lists/* /var/cache/archives/*.deb 

WORKDIR /root
